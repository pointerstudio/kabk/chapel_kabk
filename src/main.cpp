#include "ofMain.h"
#include "ofApp.h"

shared_ptr <ofApp> app;
#ifdef TARGET_EMSCRIPTEN

    void reloadVideo(string videoPath){
        ofDirectory dir("/data");
        if(dir.exists()){
            cout << "lol exists" << endl;
        }
        dir.listDir();
        for(ofFile file : dir.getFiles()){
            cout << file.getAbsolutePath() << " : " <<  endl;
        }
        app->reloadVideo(videoPath);
    }

//========================================================================
// EMSCRIPTEN BINDING BINDINGS
// here we describe classes and functions to bind them to javascript
    EMSCRIPTEN_BINDINGS(name_is_irrelevant){
        emscripten::function("reloadVideo", &reloadVideo);
    }
#endif

//========================================================================
// MAIN
// well, we have to start somewhere
int main(){
    #ifdef OF_TARGET_OPENGLES
        ofGLESWindowSettings settings;
        //settings.setSize(1920, 1080);
        settings.numSamples = 4;
        settings.glesVersion = 3;
    #else
        ofGLWindowSettings settings;
        settings.windowMode = OF_WINDOW;
        settings.setSize(1920, 1080);
        settings.setPosition(glm::vec2(1920, 0));
        settings.setGLVersion(3, 2);
    #endif
    ofCreateWindow(settings);

    app = make_shared <ofApp>();
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp(app);
}
