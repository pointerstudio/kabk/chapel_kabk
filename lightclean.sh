#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PREVIOUS_DIR=$(pwd)

cd $DIR

project=$(basename $DIR)

rm -rf bin/$project*

cd $PREVIOUS_DIR
